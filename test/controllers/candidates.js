process.env.NODE_ENV = 'test';

let Candidate = require('../../app/models/candidate');
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../../index');
let faker = require('faker');

let should = chai.should();

chai.use(chaiHttp);


describe('Candidates', () => {

  beforeEach((done) => {
    Candidate.remove({}, (err) => {
      done();
    });
  });

  describe('/POST Candidate', () => {

    it('it should POST a new Candidate', (done) => {
      let body = {
        nome: "Doria",
        profissao: "Desenvolvedor",
        localizacao: "A",
        nivel: 1
      }

      chai.request(server)
        .post('/v1/pessoas')
        .send(body)
        .end((err, res) => {
          res.should.have.status(201);
          res.body.should.be.a('object');
          res.body.should.have.property('nome');
          res.body.should.have.property('profissao');
          res.body.should.have.property('localizacao');
          res.body.should.have.property('nivel');
          done();
        });
    });

    it('it not should POST a new Candidate without name field', (done) => {
      let body = {
        profissao: "Desenvolvedor",
        localizacao: "A",
        nivel: 1
      }

      chai.request(server)
        .post('/v1/pessoas')
        .send(body)
        .end((err, res) => {
          res.should.have.status(500);
          res.body.should.be.a('object');
          res.body.should.have.property('errors')
            .have
            .property('nome')
            .have
            .property('message', 'Path `nome` is required.');

          done();
        });
    });

    it('it not should POST a new Candidate without profession field', (done) => {
      let body = {
        nome: "Doria",
        localizacao: "A",
        nivel: 1
      }

      chai.request(server)
        .post('/v1/pessoas')
        .send(body)
        .end((err, res) => {
          res.should.have.status(500);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.should.have
            .property('errors')
            .have
            .property('profissao')
            .have
            .property('message', 'Path `profissao` is required.');

          done();
        });

    });

    it('it not should POST a new Candidate without location field', (done) => {
      let body = {
        nome: "Doria",
        profissao: "Desenvolvedor",
        nivel: 1
      }

      chai.request(server)
        .post('/v1/pessoas')
        .send(body)
        .end((err, res) => {
          res.should.have.status(500);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.should.have
            .property('errors')
            .have
            .property('localizacao')
            .have
            .property('message', 'Path `localizacao` is required.');

          done();
        });

    });

    it('it not should POST a new Candidate without level field', (done) => {
      let body = {
        nome: "Doria",
        profissao: "Desenvolvedor",
        localizacao: "A",
      }

      chai.request(server)
        .post('/v1/pessoas')
        .send(body)
        .end((err, res) => {
          res.should.have.status(500);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.should.have
            .property('errors')
            .have
            .property('nivel')
            .have
            .property('message', 'Path `nivel` is required.');

          done();
        });

    });

    it('it not should POST a new Candidate without all fields', (done) => {
      let body = {

      }

      chai.request(server)
        .post('/v1/pessoas')
        .send(body)
        .end((err, res) => {
          res.should.have.status(500);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.should.have
            .property('errors')
            .have
            .property('nome')
            .have
            .property('message', 'Path `nome` is required.');
          res.body.should.have
            .property('errors')
            .have
            .property('profissao')
            .have
            .property('message', 'Path `profissao` is required.');
          res.body.should.have
            .property('errors')
            .have
            .property('localizacao')
            .have
            .property('message', 'Path `localizacao` is required.');
          res.body.should.have
            .property('errors')
            .have
            .property('nivel')
            .have
            .property('message', 'Path `nivel` is required.');
          done();
        });

    });

    it('it not should POST a new Candidate with name empty', (done) => {
      let body = {
        nome: "",
        profissao: "Desenvolvedor",
        localizacao: "A",
        nivel: 1
      }

      chai.request(server)
        .post('/v1/pessoas')
        .send(body)
        .end((err, res) => {
          res.should.have.status(500);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.should.have
            .property('errors')
            .have
            .property('nome')
            .have
            .property('message', 'Path `nome` is required.')
          res.body.should.have
            .property('errors')
            .have
            .property('nome')
            .have
            .property('value', '')
          done();
        });

    });

    it('it not should POST a new Candidate with profession empty', (done) => {
      let body = {
        nome: "Doria",
        profissao: "",
        localizacao: "A",
        nivel: 1
      }

      chai.request(server)
        .post('/v1/pessoas')
        .send(body)
        .end((err, res) => {
          res.should.have.status(500);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.should.have
            .property('errors')
            .have
            .property('profissao')
            .have
            .property('message', 'Path `profissao` is required.')
          res.body.should.have
            .property('errors')
            .have
            .property('profissao')
            .have
            .property('value', '')
          done();
        });
    });

    it('it not should POST a new Candidate with location empty', (done) => {
      let body = {
        nome: "Doria",
        profissao: "Desenvolvedor",
        localizacao: "",
        nivel: 1
      }

      chai.request(server)
        .post('/v1/pessoas')
        .send(body)
        .end((err, res) => {
          res.should.have.status(500);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.should.have
            .property('errors')
            .have
            .property('localizacao')
            .have
            .property('message', 'Path `localizacao` is required.')
          res.body.should.have
            .property('errors')
            .have
            .property('localizacao')
            .have
            .property('value', '')
          done();
        });
    });

    it('it not should POST a new Candidate with level empty', (done) => {
      let body = {
        nome: "Doria",
        profissao: "Desenvolvedor",
        localizacao: "A",
        nivel: null
      }

      chai.request(server)
        .post('/v1/pessoas')
        .send(body)
        .end((err, res) => {
          res.should.have.status(500);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.should.have
            .property('errors')
            .have
            .property('nivel')
            .have
            .property('message', 'Path `nivel` is required.')
          res.body.should.have
            .property('errors')
            .have
            .property('nivel')
            .have
            .property('value', null)
          done();
        });
    });

    it('it not should POST a new Candidate with all fields empty', (done) => {
      let body = {
        nome: "",
        profissao: "",
        localizacao: "",
        nivel: null
      }

      chai.request(server)
        .post('/v1/pessoas')
        .send(body)
        .end((err, res) => {
          res.should.have.status(500);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.should.have
            .property('errors')
            .have
            .property('nome')
            .have
            .property('message', 'Path `nome` is required.')
          res.body.should.have
            .property('errors')
            .have
            .property('nome')
            .have
            .property('value', '');

          res.body.should.have
            .property('errors')
            .have
            .property('profissao')
            .have
            .property('message', 'Path `profissao` is required.')
          res.body.should.have
            .property('errors')
            .have
            .property('profissao')
            .have
            .property('value', '')

          res.body.should.have
            .property('errors')
            .have
            .property('localizacao')
            .have
            .property('message', 'Path `localizacao` is required.')
          res.body.should.have
            .property('errors')
            .have
            .property('localizacao')
            .have
            .property('value', '')

          res.body.should.have
            .property('errors')
            .have
            .property('nivel')
            .have
            .property('message', 'Path `nivel` is required.')
          res.body.should.have
            .property('errors')
            .have
            .property('nivel')
            .have
            .property('value', null)

          done();
        });
    });

    it('it not should POST a new Candidate with more than 200 characters in the name field', (done) => {
      let body = {
        nome: faker.random.words(201),
        profissao: "Desenvolvedor",
        localizacao: "A",
        nivel: 5
      }

      chai.request(server)
        .post('/v1/pessoas')
        .send(body)
        .end((err, res) => {
          res.should.have.status(500);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.should.have
            .property('errors')
            .have
            .property('nome')
            .have
            .property('message', 'Name can not be longer than 200 characters')
          done();
        });
    });

    it('it not should POST a new Candidate with more than 100 characters in the profession field', (done) => {
      let body = {
        nome: faker.random.words(1),
        profissao: faker.random.words(100),
        localizacao: "A",
        nivel: 5
      }

      chai.request(server)
        .post('/v1/pessoas')
        .send(body)
        .end((err, res) => {
          res.should.have.status(500);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.should.have
            .property('errors')
            .have
            .property('profissao')
            .have
            .property('message', 'Profession can not be longer than 100 characters')
          done();
        });
    });
    
    it('it not should POST a new Candidate with more than 1 characters in the location field', (done) => {
      let body = {
        nome: faker.random.words(2),
        profissao: "Desenvolvedor",
        localizacao: "Ab",
        nivel: 5
      }

      chai.request(server)
        .post('/v1/pessoas')
        .send(body)
        .end((err, res) => {
          res.should.have.status(500);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.should.have
            .property('errors')
            .have
            .property('localizacao')
            .have
            .property('message', 'Location can not be longer than 1 character')
          done();
        });
    });

    it('it not should POST a new Candidate with more than 5 levels', (done) => {
      let body = {
        nome: faker.random.words(2),
        profissao: "Desenvolvedor",
        localizacao: "A",
        nivel: 6
      }

      chai.request(server)
        .post('/v1/pessoas')
        .send(body)
        .end((err, res) => {
          res.should.have.status(500);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.should.have
            .property('errors')
            .have
            .property('nivel')
            .have
            .property('message', 'Level can not be longer than 5')
          done();
        });
    });
  });



});
