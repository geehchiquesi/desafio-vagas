process.env.NODE_ENV = 'test';

let Job = require('../../app/models/job');
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../../index');
let faker = require('faker');

let should = chai.should();

chai.use(chaiHttp);


describe('Jobs', () => {

  beforeEach((done) => {
    Job.remove({}, (err) => {
      done();
    });
  });

  describe('/POST job', () => {
    
    it('it should POST a new Job', (done) => {
      let body = {
        empresa: "Vagas.com",
        titulo: "Vaga de desenvolvedor",
        descricao: "Vaga para o trabalhar no vagas.com a maior empresa de vagas do Brasil.",
        localizacao: "A",
        nivel: 3
      }

      chai.request(server)
        .post('/v1/vagas')
        .send(body)
        .end((err, res) => {
          res.should.have.status(201);
          res.body.should.be.a('object');
          res.body.should.have.property('empresa');
          res.body.should.have.property('titulo');
          res.body.should.have.property('descricao');
          res.body.should.have.property('localizacao');
          res.body.should.have.property('nivel');
          done();
        });
    });

    it('it not should POST a new Job without company name field', (done) => {
      let body = {
        titulo: "Vaga de desenvolvedor",
        descricao: "Vaga para o trabalhar no vagas.com a maior empresa de vagas do Brasil.",
        localizacao: "A",
        nivel: 3
      }

      chai.request(server)
        .post('/v1/vagas')
        .send(body)
        .end((err, res) => {
          res.should.have.status(500);
          res.body.should.be.a('object');
          res.body.should.have.property('errors')
            .have
            .property('empresa')
            .have
            .property('message', 'Path `empresa` is required.');

          done();
        });
    });

    it('it not should POST a new Job without title field', (done) => {
      let body = {
        empresa: "Vaga de desenvolvedor",
        descricao: "Vaga para o trabalhar no vagas.com a maior empresa de vagas do Brasil.",
        localizacao: "A",
        nivel: 3
      }

      chai.request(server)
        .post('/v1/vagas')
        .send(body)
        .end((err, res) => {
          res.should.have.status(500);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.should.have
            .property('errors')
            .have
            .property('titulo')
            .have
            .property('message', 'Path `titulo` is required.');

          done();
        });

    });

    it('it not should POST a new Job without description field', (done) => {
      let body = {
        titulo: "Vaga de desenvolvedor",
        empresa: "Vaga de desenvolvedor",
        localizacao: "A",
        nivel: 3
      }

      chai.request(server)
        .post('/v1/vagas')
        .send(body)
        .end((err, res) => {
          res.should.have.status(500);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.should.have
            .property('errors')
            .have
            .property('descricao')
            .have
            .property('message', 'Path `descricao` is required.');

          done();
        });

    });

    it('it not should POST a new Job without location field', (done) => {
      let body = {
        titulo: "Vaga de desenvolvedor",
        empresa: "Vaga de desenvolvedor",
        descricao: "Vaga de desenvolvedor senior",
        nivel: 3
      }

      chai.request(server)
        .post('/v1/vagas')
        .send(body)
        .end((err, res) => {
          res.should.have.status(500);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.should.have
            .property('errors')
            .have
            .property('localizacao')
            .have
            .property('message', 'Path `localizacao` is required.');

          done();
        });

    });

    it('it not should POST a new Job without level field', (done) => {
      let body = {
        titulo: "Vaga de desenvolvedor",
        empresa: "Vaga de desenvolvedor",
        descricao: "Vaga de desenvolvedor senior",
        localizacao: "A"
      }

      chai.request(server)
        .post('/v1/vagas')
        .send(body)
        .end((err, res) => {
          res.should.have.status(500);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.should.have
            .property('errors')
            .have
            .property('nivel')
            .have
            .property('message', 'Path `nivel` is required.');

          done();
        });

    });

    it('it not should POST a new Job without all fields', (done) => {
      let body = {

      }

      chai.request(server)
        .post('/v1/vagas')
        .send(body)
        .end((err, res) => {
          res.should.have.status(500);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.should.have
            .property('errors')
            .have
            .property('titulo')
            .have
            .property('message', 'Path `titulo` is required.');
          res.body.should.have
            .property('errors')
            .have
            .property('descricao')
            .have
            .property('message', 'Path `descricao` is required.');
          res.body.should.have
            .property('errors')
            .have
            .property('localizacao')
            .have
            .property('message', 'Path `localizacao` is required.');
          res.body.should.have
            .property('errors')
            .have
            .property('nivel')
            .have
            .property('message', 'Path `nivel` is required.');
          res.body.should.have
            .property('errors')
            .have
            .property('nivel')
            .have
            .property('message', 'Path `nivel` is required.');
          done();
        });

    });

    it('it not should POST a new Job with title empty', (done) => {
      let body = {
        titulo: "",
        empresa: "Vaga de desenvolvedor",
        descricao: "Vaga de desenvolvedor senior",
        localizacao: "A",
        nivel: 3
      }

      chai.request(server)
        .post('/v1/vagas')
        .send(body)
        .end((err, res) => {
          res.should.have.status(500);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.should.have
            .property('errors')
            .have
            .property('titulo')
            .have
            .property('message', 'Path `titulo` is required.')
          res.body.should.have
            .property('errors')
            .have
            .property('titulo')
            .have
            .property('value', '')
          done();
        });

    });

    it('it not should POST a new Job with company name empty', (done) => {
      let body = {
        titulo: "vaga para dev",
        empresa: "",
        descricao: "Vaga de desenvolvedor senior",
        localizacao: "A",
        nivel: 3
      }

      chai.request(server)
        .post('/v1/vagas')
        .send(body)
        .end((err, res) => {
          res.should.have.status(500);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.should.have
            .property('errors')
            .have
            .property('empresa')
            .have
            .property('message', 'Path `empresa` is required.')
          res.body.should.have
            .property('errors')
            .have
            .property('empresa')
            .have
            .property('value', '')
          done();
        });
    });

    it('it not should POST a new Job with description empty', (done) => {
      let body = {
        titulo: "vaga para dev",
        empresa: "Vagas.com",
        descricao: "",
        localizacao: "A",
        nivel: 3
      }

      chai.request(server)
        .post('/v1/vagas')
        .send(body)
        .end((err, res) => {
          res.should.have.status(500);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.should.have
            .property('errors')
            .have
            .property('descricao')
            .have
            .property('message', 'Path `descricao` is required.')
          res.body.should.have
            .property('errors')
            .have
            .property('descricao')
            .have
            .property('value', '')
          done();
        });
    });

    it('it not should POST a new Job with location empty', (done) => {
      let body = {
        titulo: "vaga para dev",
        empresa: "Vagas.com",
        descricao: "Vaga de desenvolvedor senior",
        localizacao: "",
        nivel: 3
      }

      chai.request(server)
        .post('/v1/vagas')
        .send(body)
        .end((err, res) => {
          res.should.have.status(500);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.should.have
            .property('errors')
            .have
            .property('localizacao')
            .have
            .property('message', 'Path `localizacao` is required.')
          res.body.should.have
            .property('errors')
            .have
            .property('localizacao')
            .have
            .property('value', '')
          done();
        });
    });

    it('it not should POST a new Job with level empty', (done) => {
      let body = {
        titulo: "vaga para dev",
        empresa: "vagas.com",
        descricao: "Vaga de desenvolvedor senior",
        localizacao: "A",
        nivel: null
      }

      chai.request(server)
        .post('/v1/vagas')
        .send(body)
        .end((err, res) => {
          res.should.have.status(500);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.should.have
            .property('errors')
            .have
            .property('nivel')
            .have
            .property('message', 'Path `nivel` is required.')
          res.body.should.have
            .property('errors')
            .have
            .property('nivel')
            .have
            .property('value', null)
          done();
        });
    });

    it('it not should POST a new Job with all fields empty', (done) => {
      let body = {
        titulo: "",
        empresa: "",
        descricao: "",
        localizacao: "",
        nivel: null
      }

      chai.request(server)
        .post('/v1/vagas')
        .send(body)
        .end((err, res) => {
          res.should.have.status(500);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.should.have
            .property('errors')
            .have
            .property('empresa')
            .have
            .property('message', 'Path `empresa` is required.')
          res.body.should.have
            .property('errors')
            .have
            .property('empresa')
            .have
            .property('value', '');

            res.body.should.have
            .property('errors')
            .have
            .property('titulo')
            .have
            .property('message', 'Path `titulo` is required.')
          res.body.should.have
            .property('errors')
            .have
            .property('titulo')
            .have
            .property('value', '')

            res.body.should.have
            .property('errors')
            .have
            .property('descricao')
            .have
            .property('message', 'Path `descricao` is required.')
          res.body.should.have
            .property('errors')
            .have
            .property('descricao')
            .have
            .property('value', '')

            res.body.should.have
            .property('errors')
            .have
            .property('localizacao')
            .have
            .property('message', 'Path `localizacao` is required.')
          res.body.should.have
            .property('errors')
            .have
            .property('localizacao')
            .have
            .property('value', '')

            res.body.should.have
            .property('errors')
            .have
            .property('nivel')
            .have
            .property('message', 'Path `nivel` is required.')
          res.body.should.have
            .property('errors')
            .have
            .property('nivel')
            .have
            .property('value', null)

          done();
        });
    });

    it('it not should POST a new Job with more than 200 characters in the company name field', (done) => {
      let body = {
        titulo: "vaga para dev",
        empresa: faker.random.words(201),
        descricao: "Vaga de desenvolvedor senior",
        localizacao: "A",
        nivel: 2
      }

      chai.request(server)
        .post('/v1/vagas')
        .send(body)
        .end((err, res) => {
          res.should.have.status(500);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.should.have
            .property('errors')
            .have
            .property('empresa')
            .have
            .property('message', 'Company name can not be longer than 200 characters')
          done();
        });
    });

    it('it not should POST a new Job with more than 100 characters in the title field', (done) => {
      let body = {
        titulo: faker.random.words(101),
        empresa: "Test",
        descricao: "Vaga de desenvolvedor senior",
        localizacao: "A",
        nivel: 2
      }

      chai.request(server)
        .post('/v1/vagas')
        .send(body)
        .end((err, res) => {
          res.should.have.status(500);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.should.have
            .property('errors')
            .have
            .property('titulo')
            .have
            .property('message', 'title can not be longer than 100 characters')
          done();
        });
    });

    it('it not should POST a new Job with more than 500 characters in the description field', (done) => {
      let body = {
        titulo: "vaga para dev",
        empresa: "Test",
        descricao: faker.random.words(500),
        localizacao: "A",
        nivel: 2
      }

      chai.request(server)
        .post('/v1/vagas')
        .send(body)
        .end((err, res) => {
          res.should.have.status(500);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.should.have
            .property('errors')
            .have
            .property('descricao')
            .have
            .property('message', 'description can not be longer than 500 characters')
          done();
        });
    });

    it('it not should POST a new Job with more than 1 characters in the location field', (done) => {
      let body = {
        titulo: "vaga para dev",
        empresa: "Test",
        descricao: "teste de descricao",
        localizacao: "Ab",
        nivel: 2
      }

      chai.request(server)
        .post('/v1/vagas')
        .send(body)
        .end((err, res) => {
          res.should.have.status(500);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.should.have
            .property('errors')
            .have
            .property('localizacao')
            .have
            .property('message', 'location can not be longer than 1 character')
          done();
        });
    });

    it('it not should POST a new Job with more than 5 levels', (done) => {
      let body = {
        titulo: "vaga para dev",
        empresa: "Test",
        descricao: "teste de descricao",
        localizacao: "A",
        nivel: 7
      }

      chai.request(server)
        .post('/v1/vagas')
        .send(body)
        .end((err, res) => {
          res.should.have.status(500);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.should.have
            .property('errors')
            .have
            .property('nivel')
            .have
            .property('message', 'level can not be longer than 5')
          done();
        });
    });
  });



});
