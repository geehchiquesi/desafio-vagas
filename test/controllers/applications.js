process.env.NODE_ENV = 'test';

let Application = require('../../app/models/application');
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../../index');
let faker = require('faker');
let factories = require('../factories');

let should = chai.should();

chai.use(chaiHttp);


describe('Applications', () => {

  beforeEach((done) => {
    Application.remove({}, (err) => {
      done();
    });

  });

  describe('/POST Application', () => {

    it('it should POST a new Application', async () => {
      let candidate = await factories.createNewCandidate();
      let job = await factories.createNewJob();

      let body = {
        idVaga: job.id,
        idPessoa: candidate.id
      }

      let res = await chai.request(server)
        .post('/v1/candidaturas')
        .send(body);


      res.should.have.status(201);
      res.body.should.be.a('object');
      res.body.should.have.property('idPessoa');
      res.body.should.have.property('idVaga');

    });

    it('it should POST a new Application without idVaga', async () => {
      let candidate = await factories.createNewCandidate();
      let body = {
        idPessoa: candidate.id
      }

      let res = await chai.request(server)
        .post('/v1/candidaturas')
        .send(body);

      res.should.have.status(500);
      res.body.should.be.a('object');
      res.body.should.have.property('errors')
        .have
        .property('idVaga')
        .have
        .property('message', "Path `idVaga` is required.")

    });

    it('it should POST a new Application without idPessoa', async () => {
      let job = await factories.createNewJob();
      let body = {
        idVaga: job.id,
      }

      let res = await chai.request(server)
        .post('/v1/candidaturas')
        .send(body);

      res.should.have.status(500);
      res.body.should.be.a('object');
      res.body.should.have.property('errors')
        .have
        .property('idPessoa')
        .have
        .property('message', "Path `idPessoa` is required.")

    });

    it('it should POST a new Application without all fields', async () => {
      let body = {

      }

      let res = await chai.request(server)
        .post('/v1/candidaturas')
        .send(body);

      res.should.have.status(500);
      res.body.should.be.a('object');
      res.body.should.have.property('errors')
        .have
        .property('idPessoa')
        .have
        .property('message', "Path `idPessoa` is required.");
      res.body.should.have.property('errors')
        .have
        .property('idVaga')
        .have
        .property('message', "Path `idVaga` is required.")
    });

    it('it should POST a new Application with idVaga empty', async () => {
      let candidate = await factories.createNewCandidate();

      let body = {
        idVaga: '',
        idPessoa: candidate.id
      }

      let res = await chai.request(server)
        .post('/v1/candidaturas')
        .send(body);

      res.should.have.status(500);
      res.body.should.be.a('object');
      res.body.should.have.property('errors')
        .have
        .property('idVaga')
        .have
        .property('message', "Cast to ObjectID failed for value \"\" at path \"idVaga\"");

    });

    it('it should POST a new Application with idPessoa empty', async () => {
      let job = await factories.createNewJob();

      let body = {
        idVaga: job.id,
        idPessoa: ''
      }

      let res = await chai.request(server)
        .post('/v1/candidaturas')
        .send(body);

      res.should.have.status(500);
      res.body.should.be.a('object');
      res.body.should.have.property('errors')
        .have
        .property('idPessoa')
        .have
        .property('message', "Cast to ObjectID failed for value \"\" at path \"idPessoa\"");

    });

    it('it should POST a new Application with all fields empty', async () => {

      let body = {
        idVaga: '',
        idPessoa: ''
      }

      let res = await chai.request(server)
        .post('/v1/candidaturas')
        .send(body);

      res.should.have.status(500);
      res.body.should.be.a('object');
      res.body.should.have.property('errors')
        .have
        .property('idVaga')
        .have
        .property('message', "Cast to ObjectID failed for value \"\" at path \"idVaga\"");

      res.body.should.have.property('errors')
        .have
        .property('idPessoa')
        .have
        .property('message', "Cast to ObjectID failed for value \"\" at path \"idPessoa\"");

    });

    it('it should POST a new Application when idVaga not exists', async () => {
      let candidate = await factories.createNewCandidate();
      let body = {
        idVaga: '5ba68633adba3324352c7c8c',
        idPessoa: candidate.id
      }

      let res = await chai.request(server)
        .post('/v1/candidaturas')
        .send(body);

      res.should.have.status(500);
      res.body.should.be.a('object');
      res.body.should.have.property('errors')
        .have
        .property('idVaga')
        .have
        .property('message', "Não existe essa vaga");
    });

    it('it should POST a new Application when idVaga not exists', async () => {
      let job = await factories.createNewJob();
      let body = {
        idVaga: job.id,
        idPessoa: '5ba68633adba3324352c7c8c'
      }

      let res = await chai.request(server)
        .post('/v1/candidaturas')
        .send(body);

      res.should.have.status(500);
      res.body.should.be.a('object');
      res.body.should.have.property('errors')
        .have
        .property('idPessoa')
        .have
        .property('message', "Não existe essa pessoa");
    });
  });

});
