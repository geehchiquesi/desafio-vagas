const Candidate = require('../../app/models/candidate');
const Job = require('../../app/models/job');

const factories = {
  createNewCandidate: function(){
    return Candidate.create({
      nome: "Doria",
      profissao: "Desenvolvedor",
      localizacao: "A",
      nivel: 2
    })  
  },

  createNewJob: function(){
    return Job.create({ 
      empresa: "asdddd",
      titulo: "Vaga de desenvolvedor",
      descricao: "Vaga para o trabalhar no vagas.com a maior empresa de vagas do Brasil.",
      localizacao: "A",
      nivel: 3
    })  
  }
}

module.exports = factories