'use strict'

const _ = require('lodash');

/** @module scoreCalculator */
const scoreCalculator = {
  /**
   * Realiza o calculo do score e retorna um numero inteiro
   * 
   */
  calculator: function calculator(job, candidate) {
    const distance = returnValueDistance(job.localizacao, candidate.localizacao);
    const nivel = 100 - 25 * (job.nivel - candidate.nivel);
    let score = (nivel + distance) / 2;
    score = parseInt(score);
    return score;
  },

  score: function score(entities) {
    entities = entities.map(entity =>{
      let job = entity.idVaga;
      let candidate = entity.idPessoa;
      let score = this.calculator(job, candidate);
      candidate.score = score
      return _.pick(candidate, ["nome","profissao", "localizacao", "nivel", "score"]);
    });

    return _.orderBy(entities,["score"],["desc"])
  }
}

/* ****************************************************** *
 *  PRIVATE ATTRIBUTES
 * ****************************************************** */

const distances = {
  "a": {
    "a": 0,
    "b": 5,
    "c": 12,
    "d": 8,
    "e": 16,
    "f": 16
  },
  "b": {
    "a": 5,
    "b": 0,
    "c": 7,
    "d": 3,
    "e": 11,
    "f": 11
  },
  "c": {
    "a": 12,
    "b": 7,
    "c": 0,
    "d": 10,
    "e": 4,
    "f": 18
  },
  "d": {
    "a": 8,
    "b": 3,
    "c": 10,
    "d": 0,
    "e": 10,
    "f": 8
  },
  "e": {
    "a": 16,
    "b": 11,
    "c": 4,
    "d": 10,
    "e": 0,
    "f": 18
  },
  "f": {
    "a": 16,
    "b": 11,
    "c": 18,
    "d": 8,
    "e": 18,
    "f": 0
  }
}

/* ****************************************************** *
 *  PRIVATE FUNCTIONS
 * ****************************************************** */

function returnValueDistance(jobLocation, candidateLocation) {
  const distance = distances[jobLocation.toLowerCase()][candidateLocation.toLowerCase()];
  let value;
  switch (true) {
    case (distance <= 5):
      value = 100;
      break;
    case (distance <= 10):
      value = 75;
      break;
    case (distance <= 15):
      value = 50;
      break;
    case (distance <= 20):
      value = 25;
      break;
    case (distance > 20):
      value = 0;
      break;
    default:
      throw new Error('Não é um valor valido');
      break;
  }
  return value;
}

module.exports = scoreCalculator;
