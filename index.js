'use strict'

const http = require('http');
const app = require('./app/initializers/server');
const errorHandler = require('./app/controllers/middleware/errorHandler');


// gera o erro 404 e passa adiante
app.use(function (req, res, next) {
	res.status('404').json({message: 'The requested resource could not be found.'});
});

app.use(errorHandler);

// escuta na porta
if (app.get('env') !== 'test') {
	const server = http.createServer(app);
	const port = process.env.PORT || 3000;
	app.set('port', port);
	server.listen(port);
	server.on('error', console.error);
}

module.exports = app; // for testing
