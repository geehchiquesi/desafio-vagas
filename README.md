# Desafio Vagas.com

O desafio era criar uma aplicação REST para poder cadastrar candidatos e vagas.

# Documentação do projeto

[Documentacao](https://bitbucket.org/geehchiquesi/desafio-vagas/wiki/Documentacao)

Tudo sobre como instalar e gerar um build para publicar em produção

# Variaveis de ambiente

| Variável         | Descrição                                                                       |
| ---------------- | ------------------------------------------------------------------------------- |
| NODE_ENV         | Ambiente que o projeto deve rodar (development, test, production)               |
| MONGO_HOST       | Host da base MongoDB                                                            |
| MONGO_PORT       | Porta da base MongoDB                                                           |
| MONGO_DB         | Nome da base de dados MongoDB                                                   |
| MONGO_USER       | Usuário da base MongoDB                                                         |
| MONGO_PASS       | Senha da base MongoDB                                                           |

# Instalação

O projeto usa a versão V8.12.0 LTS e pode ser baixada via NVM 

## Como instalar o NVM e o Node

Manual de instalação do proprio nvm https://github.com/creationix/nvm#git-install

após instalado você apenas tem que instalar a versão que o projeto utiliza

```sh
$ nvm install 8.12.0
```

# Configuração

Todas as configurações ficam em app/config.

# Primeira instalação

Basta executar o seguinte comando para criar a estrutura necessária para o serviço.

Dentro do projeto execute: 

```sh
$ npm install
```

instale o pm2 global para poder iniciar a aplicação

```sh
$ npm install pm2 --global
```

# Executar os testes

```sh
$ npm test
```
# Como iniciar a Aplicação

### PM2

```sh
$ pm2 start desafio-vagas.pm2.json
```

### NPM

```sh
$ npm start
```