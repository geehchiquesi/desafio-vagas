const express = require('express');
const router = express.Router();
const Application = require('../models/application');

router
  .post('/', applicationsCreateController);

function applicationsCreateController(req, res, next){
  Application.create(req.body)
  .then(entity =>{
    res.status(201).json(entity);
  })
  .catch((err) => {
    res.status(500).json(err);
  });
}

module.exports = router;