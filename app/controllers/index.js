'use strict';

const appRoot = require('app-root-path');
const express = require('express');
const pkg = require(`${appRoot}/package.json`);
const router = express.Router();

router
  .get('/', indexController);

/**
 * GET /
 *
 *  Exibe a versão da API.
 */
function indexController(req, res) {
  res.json({
    name: pkg.name,
    version: pkg.version,
    releaseDate: pkg.releaseDate,
    message: 'API is running...'
  });
}

module.exports = router;
