const express = require('express');
const router = express.Router();
const Candidate = require('../models/candidate');

router
  .post('/', candidatesCreateController);

function candidatesCreateController(req, res, next){
  Candidate.create(req.body)
  .then(entity =>{
    res.status(201).json(entity);
  })
  .catch(next);
}

module.exports = router;