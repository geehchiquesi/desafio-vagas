const express = require('express');
const router = express.Router();
const Job = require('../models/job');
const Application = require('../models/application');
const ScoreCalculator = require('../../lib/scoreCalculator');

router
  .post('/', jobsCreateController)
  .get('/:id/candidaturas/ranking', jobsShowRankingController);

function jobsCreateController(req, res, next){
  Job.create(req.body)
  .then(entity =>{
    res.status(201).json(entity);
  })
  .catch(next);
}

function jobsShowRankingController(req, res, next){
  Application
  .find({idVaga: req.params.id})
  .populate('idVaga')
  .populate('idPessoa')
  .then(entities => {
    return ScoreCalculator.score(entities);
  })
  .then(entities => {res.status(200).json(entities)})
  .catch(next);
}

module.exports = router;