module.exports = function(err, req, res, next){
  // envia a resposta
  res.status(err.status || 500);
  res.json(err);
}