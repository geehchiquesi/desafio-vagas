'use strict';

const environments = {
  development: {
    host: process.env.MONGO_HOST || 'localhost',
    port: process.env.MONGO_PORT || 27017,
    db: 'vagas_development',
    user: process.env.MONGO_USER,
    pass: process.env.MONGO_PASS
  },
  test: {
    host: process.env.MONGO_HOST || 'localhost',
    port: process.env.MONGO_PORT || 27017,
    db: 'vagas_test',
    user: process.env.MONGO_USER,
    pass: process.env.MONGO_PASS
  },
  production: {
    host: process.env.MONGO_HOST || 'localhost',
    port: process.env.MONGO_PORT || 27017,
    db: process.env.MONGO_DB || 'vagas',
    user: process.env.MONGO_USER,
    pass: process.env.MONGO_PASS
  }
};

// environment variables
const dbConfig = environments[process.env.NODE_ENV || 'development'];

module.exports = dbConfig;
