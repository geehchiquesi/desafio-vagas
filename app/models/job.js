'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const JobSchema = new Schema({
  empresa: { 
    type: String, 
    required: true,
    min: 1,
    max: 200
  },
  titulo: {
    type: String, 
    required: true,
    min:10,
    max: 100
  },
  descricao: {
    type: String, 
    required: true,
    min: 20,
    max: 500
  },
  localizacao: {
    type: String, 
    required: true,
  },
  nivel: {
    type: Number, 
    required: true,
  },
  createdAt: { type: Date, default: Date.now },
  updatedAt: Date
});

// Middleware
JobSchema.pre('validate', function (next) {
  this.updatedAt = Date.now();
  next();
});

// Validation

JobSchema.path('empresa').validate((empresa) =>{
  return empresa.length <= 200
},'Company name can not be longer than 200 characters');

JobSchema.path('titulo').validate((titulo) =>{
  return titulo.length <= 100
},'title can not be longer than 100 characters');

JobSchema.path('descricao').validate((descricao) =>{
  return descricao.length <= 100
},'description can not be longer than 500 characters');

JobSchema.path('localizacao').validate((localizacao) =>{
  return localizacao.length <= 1
},'location can not be longer than 1 character');

JobSchema.path('nivel').validate((nivel) =>{
  return nivel <= 5 && nivel >=1
},'level can not be longer than 5');




// Serialization
JobSchema.options.toJSON = {
  transform: function (doc, ret) {
    delete ret.__v;
    return ret;
  }
};

const Job = mongoose.model('Job', JobSchema);

module.exports = Job;