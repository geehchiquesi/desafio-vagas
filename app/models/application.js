'use strict';

const mongoose = require('mongoose');
const ObjectId = require('mongoose').Types.ObjectId;
const Schema = mongoose.Schema;
const Candidate = require('./candidate');
const Job = require('./job');
const _ = require('lodash');

const ApplicationSchema = new Schema({
  idVaga: {type: Schema.Types.ObjectId, required: true, ref: 'Job'  },
  idPessoa: {type: Schema.Types.ObjectId, required: true,  ref: 'Candidate' },
  createdAt: { type: Date, default: Date.now },
  updatedAt: Date
});

// Middleware
ApplicationSchema.pre('validate', function (next) {
  this.updatedAt = Date.now();
  next();
});

// Serialization
ApplicationSchema.options.toJSON = {
  transform: function (doc, ret) {
    delete ret.__v;
    return ret;
  }
};

// Custom Validations
ApplicationSchema.path('idVaga').validate(function (idVaga) {
  return Job
    .findOne({_id: new ObjectId(idVaga._id)})
    .then(entity => { 
      return _.includes(entity._id.toString(),idVaga._id.toString());
    })
}, 'Não existe essa vaga');

ApplicationSchema.path('idPessoa').validate(function (idPessoa) {
  return Candidate
    .findOne({_id: new ObjectId(idPessoa._id)})
    .then(entity => { 
      return _.includes(entity._id.toString(), idPessoa._id.toString()) 
    })
}, 'Não existe essa pessoa');


const Application = mongoose.model('Application', ApplicationSchema);

module.exports = Application;