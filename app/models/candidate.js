'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CandidateSchema = new Schema({
  nome: { 
    type: String, 
    required: true,
    min: 1,
    max: 200
  },
  profissao: {
    type: String, 
    required: true,
    min: 10,
    max: 100
  },
  localizacao: {
    type: String, 
    required: true,
  },
  nivel: {
    type: Number, 
    required: true,
  },
  createdAt: { type: Date, default: Date.now },
  updatedAt: Date
});

// Middleware
CandidateSchema.pre('validate', function (next) {
  this.updatedAt = Date.now();
  next();
});

// Validation

CandidateSchema.path('nome').validate((nome) => {
  return nome.length <= 200;
}, 'Name can not be longer than 200 characters')

CandidateSchema.path('profissao').validate((profissao) => {
  return profissao.length <= 100;
}, 'Profession can not be longer than 100 characters')

CandidateSchema.path('localizacao').validate((localizacao) => {
  return localizacao.length <= 1;
}, 'Location can not be longer than 1 character')

CandidateSchema.path('nivel').validate((nivel) => {
  return nivel <=5 && nivel >=1;
}, 'Level can not be longer than 5')


// Serialization
CandidateSchema.options.toJSON = {
  transform: function (doc, ret) {
    delete ret.__v;
    return ret;
  }
};

const Candidate = mongoose.model('Candidate', CandidateSchema);

module.exports = Candidate;