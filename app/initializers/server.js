'use strict';

const consign = require('consign');
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const connectDb = require('./database');

// instância da aplicação
const app = express();
app.disable('x-powered-by');

// define parsers
app.use(bodyParser.json({ limit: '10mb' }));
app.use(bodyParser.urlencoded({ extended: false }));

// conecta ao banco de dados
connectDb(mongoose, app.get('env'));

// carrega as rotas e a conexao com o banco
consign()
  .include('app/initializers/routes.js')
  .then('app/initializers/database.js')
  .into(app);

module.exports = app;
