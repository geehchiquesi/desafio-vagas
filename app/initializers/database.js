const mongoose = require('mongoose');
const appRoot = require('app-root-path');
const fs = require('fs');
const path = require('path');
const database = require('../config/database');

mongoose.Promise = require('bluebird');

module.exports = function () {
  const uri = 'mongodb://' + database.host + '/' + database.db;

  mongoose.connect(uri,{ useNewUrlParser: true })
    .then(() => {
      // carrega todas as models ao iniciar
      const modelsDir = `${appRoot}/app/models`;
      const models = fs.readdirSync(modelsDir).filter(fileName => path.extname(fileName) === '.js');
      models.forEach((model) => require(`${modelsDir}/${model}`));
    });
};
