module.exports = function (app) {
	// usa os controllers
	app.use('/', require('../controllers'));
	app.use('/v1/vagas', require('../controllers/jobs'));
	app.use('/v1/pessoas', require('../controllers/candidates'));
	app.use('/v1/candidaturas', require('../controllers/applications'));
}